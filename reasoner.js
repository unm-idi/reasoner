/*!
// @name         Nervous Grace
// @namespace    http://idi.unm.edu
// @author       Ricardo Piro-Rael
// @version      0.4.7
// @description  Reasoner for Serialized Stage
*/

(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/* Base class
 * This contains methods that are used in more than one other class.
 */
class Base {
  // The type of the object
  get type() {
    this.constructor.class;
  }

  // Sometimes this needs to be used as a ``function'', rather than as an
  // attribute. This sums the value for an array of courses.
  static abs(courses) {
    return courses.reduce((a, e) => a + (e.hours || 0), 0);
  }

  static json_from_keys(item) {
    var json_object = {};
    Object.keys(item).forEach(key => json_object[key] = item[key]);
    return json_object;
  }

}

module.exports = Base;

},{}],2:[function(require,module,exports){
/* Attributes:
 * name: String, the name of the requirement
 * min_hours: Integer, the minimum number of hours to satisfy the requirement
 * max_hours: Integer, the maximum number of hours the requirment may provide
 * take: Integer, the minimum number of subrequirements to be satisfied
 * subrequirements: Array of subrequirements (requirements and courses)
 */

Base = require('./base.js');

class CheckedRequirement extends Base {
  constructor(name, min_hours, max_hours, take, subrequirements) {
    super();
    this.name                    = name;
    this.min_hours               = min_hours;
    this.max_hours               = max_hours;
    this.take                    = take;
    this.subrequirements         = subrequirements;
    this.checked_subrequirements = [];
  }

  /* The algorithm specifies a need to know whether or not a requirement is
   * ``basic''. This means that every subrequirement is a course.
  get basic() {
    return this.subs.every(sub => sub.type == 'Course');
  }
  */

  // How many subrequirements did the student satisfy?
  get taken() {
    return this.satisfied_subrequirements.length;
  }

  // How many credit hours is this subrequirement worth independently?
  // get abs() {
    // return this.subrequirements.map(sub => sub.abs).reduce((a, e) => a + e, 0); 
  // }

  // Is this requirement satisfied (represented as \vdash)?
  get satisfied() {
    return this.taken >= this.take && this.hours >= this.min_hours;
  }

  // A helper method to get all the satisfied subrequirements
  get satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => requirement.satisfied);
  }
  //
  // A helper method to get all the satisfied subrequirements
  get partially_satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => 
      !requirement.satisfied && (requirement.hours > 0 && requirement.taken > 0)
    );
  }
  
  // A helper method to get all the not satisfied subrequirements
  get not_satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => 
      !requirement.satisfied && !(requirement.hours > 0 && requirement.taken > 0)
    );
  }

  // The courses in this set, but not the other set (name ``requirement'')
  setminus(requirement) {
    return this.subrequirements.filter(course => requirement.subrequirements.indexOf(course) < 0);
  }

  // The courses in either this set, or the other set (named ``requirement'')
  cup(requirement) {
    return [...new Set([...this.subrequirements, ...requirement.subrequirements])];
  }

  // The courses in both this set, and the other set (named ``requirement'')
  cap(requirement) {
    return this.subrequirements.filter(course => requirement.subrequirements.indexOf(course) >= 0);
  }

  // Alias method for backwards compatibility
  get credit_hours_required() {
    return this.min_credit_hours;
  }

  // Alias method for backwards compatibility
  get hours_satisfied() {
    return this.hours;
  }

  // The merge (\merge) method. This is a binary operator.
  merge(requirement) {
    // Hours in this set or sum of course hours
    var hours = Math.min(Math.min(Base.abs(this.subrequirements), Base.abs(this.setminus(requirement))) +
        // Hours in the peer set or sum of course hours
        Math.min(requirement.hours, Base.abs(requirement.setminus(this))) +
        // Hours in courses that are in both
        Base.abs(this.cap(requirement)),
        // Sum of nominal hours of both requirements.
        this.max_hours + requirement.max_hours);
    // Subrequirements now has courses from both
    var subrequirements = this.cup(requirement);
    // Return the new requirement (probably going to keep merging)
    var replacement = new CheckedRequirement(this.name + ", " + requirement.name, hours, hours, 0, subrequirements);
    replacement.hours = hours;
    return replacement;
  }

  // Now that the unused courses have been filtered, reduce
  reason() {
    // Reason each one
    var replacement = this.subrequirements.map(sub => sub.reason())
      // Keep the satisfied ones
      .filter(sub => sub.satisfied)
      // Merge
      .reduce((acc, sub) => acc.merge(sub), new CheckedRequirement("", 0, 0, 0, []));
    return replacement;
  }

  toJSON() {
    var json_object = Base.json_from_keys(this);
    ['satisfied_subrequirements',
     'partially_satisfied_subrequirements',
     'not_satisfied_subrequirements',
     'taken',
     'satisfied',
     'credit_hours_required',
     'hours_satisfied'
    ].forEach(method => json_object[method] = this[method]);
    ['checked_subrequirements',
     'hours',
     'max_hours'
    ].forEach(key => delete(json_object[key]));
    json_object.type = String(this.kind || this.type).toLowerCase();
    return json_object;
  }

}

module.exports = CheckedRequirement;

},{"./base.js":1}],3:[function(require,module,exports){
/* Course class
 * Attributes:
 * name: String, name of the course
 * min_hours: Minimum number of hours the course may provide
 * max_hours: Maximum number of hours the course may provide
 */

Base = require('./base.js');

class Course extends Base {
  constructor(course, min_grade, attempts) {
    super();
    this.name      = course.full_number;
    this.min_hours = course.minimum_credits;
    this.max_hours = course.maximum_credits;
    this.min_grade = min_grade;
    this.attempts  = attempts;
  }

  /* This is the satisfy method specified in the algorithm. Check whether or
   * not the student has taken the course, and whether or not the minimum grade
   * is satisfied.
   */
  vdash(student) {
    // A sort of ``empty'' requirement
    var replacement = new CheckedRequirement(this.name, this.min_hours, this.max_hours, 1, []);
    /* Find courses that matches this one, including min grade (simeq). Then,
     * sort so the highest grades come first. Take only as many as attempts are
     * required.
     */
    var student_courses = student.courses.filter(course => course.simeq(this))
      .sort((a, b) => b.grade.preceq(a.grade)).slice(0, (this.attempts || 1));
    if(student_courses.length) {
      // The hours should be the sum of the total
      replacement.hours = student_courses.map(course => course.hours).reduce((a, e) => a + e);
      // Take all courses that were use and pass them up
      replacement.subrequirements = student_courses;
      replacement.checked_subrequirements = student_courses;
    } else {
      // Nothing was satisfied, so pass a zero
      replacement.hours = 0;
    }
    replacement.kind = 'course';
    return replacement;
  }

}

module.exports = Course;

},{"./base.js":1}],4:[function(require,module,exports){
/* Grade class
 * Attributes:
 * letter: String, the letter grade
 * order: Integer, where this ranks in the partially ordered set
 */

Base  = require("./base.js");
Order = require("./grade_order.json");

class Grade extends Base {
  constructor(letter) {
    super();
    this.letter = letter;
    this.order  = Grade.grade_order(letter);
    this.cr     = letter == "CR"
  }

  // Compare this to see if requirement's grade precedes the student's grade
  preceq(grade) {
    return (this.order <= grade.order) && (this.cr || !grade.cr);
  }

  static grade_order(letter) {
    return Order[letter] || Order["C"];
  }
}

module.exports = Grade;

},{"./base.js":1,"./grade_order.json":5}],5:[function(require,module,exports){
module.exports={
  "CR": 1,
  "D-": 2,
  "D": 3,
  "D+": 4,
  "C-": 5,
  "C": 6,
  "C+": 7,
  "B-": 8,
  "B": 9,
  "B+": 10,
  "A-": 11,
  "A": 12,
  "A+": 13
}
},{}],6:[function(require,module,exports){
/* Requirement Class
 * This class is the requirements that come directly from the API.
 * Attributes:
 * name: String, the name of the requirement
 * min_hours: Integer, the minimum hours required to satisfy the requirement
 * max_hours: Integer, the maximum hours the requirement may provide
 * take: Integer, the number of subrequirements needed to satisfy this
 * subrequirements: Array of subrequirements (Requirements and Courses).
 */

Base               = require("./base.js");
Grade              = require("./grade.js");
CheckedRequirement = require("./checked_requirement.js");
Course             = require("./course.js");
Wildcard           = require("./wildcard.js");

class Requirement extends Base {
  constructor(name, min_hours, max_hours, min_grade, take, subrequirements) {
    super();
    // Take the arguments and use them as attributes
    this.name            = name;
    this.min_hours       = min_hours;
    this.max_hours       = max_hours;
    // Transform this into a Grade object
    this.min_grade       = new Grade(min_grade);
    this.take            = take;
    this.subrequirements = subrequirements;

    // Take the courses, and add the min_grade to each one
    this.courses = this.pre_courses.map(course => new Course(course.course, this.min_grade, course.attempts));

    this.wildcards = this.pre_wildcards.map(wildcard => 
      new Wildcard(
        wildcard.name,
        wildcard.minimum_credits,
        wildcard.maximum_credits,
        wildcard.min_grade,
        wildcard.rules
      )
    );

    // Change the subrequirements into Requirement objects
    this.subrequirements = this.subs.map(sub =>
      new Requirement(
        sub.name,
        sub.minimum_credits,
        sub.maximum_credits,
        sub.min_grade,
        (sub.take || 1),
        sub.subrequirements
      )
    );
  }

  /* The algorithm specifies a need to know whether or not a requirement is
   * ``basic''. This means that every subrequirement is a course.
   */
  get basic() {
    return this.subs.every(sub => sub.type.toLowerCase() == 'course');
  }

  // Only Course subrequirements, from the arguments
  get pre_courses() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'course');
  }

  // Only Requirement subrequirements
  get subs() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'requirement');
  }

  get pre_wildcards() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'wildcard');
  }

  // We need all the subrequirements in a single array for reasoning
  get full_subrequirements() {
    return this.subrequirements.concat(this.wildcards).concat(this.courses);
  }

  /* This is the satisfy method specified in the algorithm. Return a
   * CheckedRequirement.
   */
  vdash(student) {
    // Run this method on every subrequirement
    var checked = this.full_subrequirements.map(sub => sub.vdash(student));
    // Filter out the unsatisfied ones and merge (don't actually filter for now)
    var merged = checked
      //.filter(sub => sub.satisfied)
      .reduce((acc, sub) => 
      acc.merge(sub), new CheckedRequirement("", 0, 0, this.take, [])
    );
    var replacement = new CheckedRequirement(
        this.name,
        this.min_hours,
        this.max_hours,
        this.take,
        merged.subrequirements
    );
    replacement.hours = Math.min(this.max_hours, merged.hours);
    replacement.checked_subrequirements = checked;
    return replacement;
  }

}

module.exports = Requirement;

},{"./base.js":1,"./checked_requirement.js":2,"./course.js":3,"./grade.js":4,"./wildcard.js":11}],7:[function(require,module,exports){
/* Rule class
 * These are the rules to be matched against for the wildcard
 * Attributes:
 * code_string: The name of the department(s) to be matched against
 * match_string: The string corresponding to the entire name(s) of the courses
 * minimum_match_number: The lowest course number to be matched against
 * maximum_match_number: The highest course number to be matched against
 * min_hours: The lowest acceptable hours value for a course
 * max_hours: The highest acceptable hours value for a course
 * exclude: Courses matching this rule should be excluded from the set
 */
Base = require("./base.js");

class Rule extends Base {
  constructor(
    code_string,
    match_string,
    minimum_match_number,
    maximum_match_number,
    min_hours,
    max_hours,
    exclude,
    min_grade
  ) {
    super();
    this.code_string          = code_string;
    this.match_string         = match_string;
    this.minimum_match_number = minimum_match_number;
    this.maximum_match_number = maximum_match_number;
    this.min_hours            = min_hours;
    this.max_hours            = max_hours;
    this.exclude              = exclude;
    this.min_grade            = min_grade;
  }

  // We are using "splats" here, convert them to regular expression syntax
  splat_to_regex(splat) {
    return splat.replace(/\*|\?/g, char => char == "*" ? ".*" : ".")
  }

  // Check if string matches a pattern
  match(string, splat) {
    return !string || !!(string.match(this.splat_to_regex(splat)));
  }

  number_between(course_number) {
    return (!this.minimum_match_number || this.minimum_match_number <= course_number) && 
           (!this.maximum_match_number || this.maximum_match_number >= course_number);
  }

  hours_between(course_hours) {
    return (!this.min_hours || this.min_hours <= course_hours) && 
           (!this.max_hours || this.max_hours >= course_hours);
  }

  // Check if a single course matches the rule
  simeq(course) {
    return [
      this.match(course.name.split(" ")[0], this.code_string),
      this.match(course.name, this.match_string),
      this.number_between(parseInt(course.name.split(" ").pop())),
      this.hours_between(course.hours),
      this.min_grade.preceq(course.grade)
    ].every(Boolean);
  }

  // Get all courses that match this wildcard
  matches(courses) {
    return courses.filter(course => this.simeq(course))
  }

}

module.exports = Rule;

},{"./base.js":1}],8:[function(require,module,exports){
/* Student class
 * Represents a student
 * Attributes:
 * name: String, name of the student (or alias)
 * courses: Array, courses that students have taken
 */

Base          = require("./base.js");
StudentCourse = require("./student_course.js");

class Student extends Base {
  constructor(courses) {
    super();
    this.courses = courses.map(course => new StudentCourse(course.name, course.hours, course.grade));
  }

  // Get the courses that are in common with a requirement
  cap(requirement) {
    return courses.filter(course => 
        requirement.subrequirements.map(c => course.simeq(c)) &&
        requirement.min_grade.preceq(course.grade)
    );
  }

  // Check if the student satisfied a requirement
  vdash(requirement) {
    var tree = requirement.vdash(this);
    return tree.satisfied;
  }
}

module.exports = Student;

},{"./base.js":1,"./student_course.js":9}],9:[function(require,module,exports){
/* StudentCourse Class
 * Attrubutes:
 * name: String, the name of the course
 * hours: The number of credit hours the student receives
 * grade: The grade the student received
 */

Base = require("./base.js");

class StudentCourse extends Base {
  constructor(name, hours, grade) {
    super();
    this.name      = name;
    this.hours     = hours;
    this.grade     = new Grade(grade);
  }

  // Check if this is the same as an API course
  simeq(course) {
    return this.name == course.name && course.min_grade.preceq(this.grade);
  }

  // Used as an alias for self, for polymorphism with merge method
  get subrequirements() {
    return [this];
  }

  // Used as an alias for true for the merge method
  get satisfied() {
    return true;
  }

  toJSON() {
    var json_object = Base.json_from_keys(this);
    json_object.grade = (this.grade && this.grade.letter) || 'Unknown';
    return json_object;
  }
}

module.exports = StudentCourse;

},{"./base.js":1}],10:[function(require,module,exports){
var Base               = require('./base.js');
var CheckedRequirement = require('./checked_requirement.js');
var Course             = require('./course.js');
var Grade              = require('./grade.js');
var Requirement        = require('./requirement.js');
var Rule               = require('./rule.js');
var Student            = require('./student.js');
var StudentCourse      = require('./student_course.js');
var Wildcard           = require('./wildcard.js');

function student_json(courses) {
  return new Student(courses);
}

function requirement_json(requirement_roots) {
  var requirements = requirement_roots.map(requirement_root =>
    new Requirement(
      "Requirement Root",
      requirement_root.total_credit_hours, 
      requirement_root.total_credit_hours, 
      requirement_root.min_grade || 'C',
      requirement_root.take || requirement_root.requirements.length,
      requirement_root.requirements
    )
  );
  var major = requirements.find(req => 
    req.min_hours == Math.max(...requirements.map(req => req.min_hours))
  );
  major.subrequirements = major.subrequirements.concat(requirements.filter(req => major != req));
  return major;
}

function reason(student, requirements) {
  return requirement_json(requirements).vdash(student_json(student));
}

// module.exports = { Base, CheckedRequirement, Course, Grade, Requirement, Student, StudentCourse, Wildcard, Rule, student_json, requirement_json, reason };
module.exports = reason;

},{"./base.js":1,"./checked_requirement.js":2,"./course.js":3,"./grade.js":4,"./requirement.js":6,"./rule.js":7,"./student.js":8,"./student_course.js":9,"./wildcard.js":11}],11:[function(require,module,exports){
/* This contains Wildcards to match courses against. Each Wildcard is a set a of
 * rules, which contain attributes to match courses against.
 * Attributes:
 * name: String, the name of the Wildcard
 * min_hours: The minimum hours required to satisfy the Wildcard
 * max_hours: The maximum hours the Wildcard may provide to the Requirement
 * rules: An array of rules to be matches against
 */
Base               = require("./base.js");
Rule               = require("./rule.js");
CheckedRequirement = require("./checked_requirement.js");

class Wildcard extends Base {
  constructor(name, min_hours, max_hours, min_grade, rules) {
    super();
    this.name       = name;
    this.min_hours  = min_hours;
    this.max_hours  = max_hours;
    this.min_grade  = new Grade(min_grade);
    this.rules      = rules.map(rule =>
      new Rule(
        rule.code_string,
        rule.match_string,
        rule.minimum_match_number,
        rule.maximum_match_number,
        (rule.min_hours || 1),
        (rule.max_hours || this.max_hours),
        rule.exclude,
        this.min_grade
      )
    );
  }

  // Requirements to be included in the set
  get includes() {
    return this.rules.filter(rule => !rule.exclude);
  }

  // Requirements to be excluded from the set
  get excludes() {
    return this.rules.filter(rule => rule.exclude);
  }

  // Union of two course sets
  cup(set_one, set_two) {
    return [...new Set([...set_one, ...set_two])];
  }

  // Courses that match
  match(courses) {
    // Find all the courses to include
    var included_courses = this.includes.map(rule => rule.matches(courses)).reduce((a, e) => this.cup(a, e), []);
    // Find all the courses to exclude
    var excluded_courses = this.excludes.map(rule => rule.matches(courses)).reduce((a, e) => this.cup(a, e), []);
    // Return all the included courses without the excluded ones
    return included_courses.filter(course => excluded_courses.indexOf(course) < 0 && this.min_grade.preceq(course.grade));
  }

  vdash(student) {
    var replacement = new CheckedRequirement(this.name, this.min_hours, this.max_hours, 1, this.match(student.courses));
    replacement.hours = Base.abs(this.match(student.courses));
    replacement.checked_subrequirements = this.match(student.courses);
    replacement.kind = 'wildcard';
    return replacement;
  }

}

module.exports = Wildcard;

},{"./base.js":1,"./checked_requirement.js":2,"./rule.js":7}]},{},[10]);
