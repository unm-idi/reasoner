# Nervous Grace

This is the JavaScript version of the reasoner for serialized stage. This is
designed to be used either with NodeJS or with a browser. In either case, it
requires ES6, so it requires Node 6 or higher. As of this writing, it has been
tested with Node 7.2.1.

To use this, require it and pass in an array containing a student, and an array
of objects for the requirements.

``` javascript
reasoner = require("nervous-grace");
         // Student Objects                             // Requirements
reasoner([{name: "Course Name", grade: "G", hours: 1}], [ major, core ]);
```

To run the tests, do the following:

``` javascript
node test/test.js
```
