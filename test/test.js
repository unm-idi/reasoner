reasoner = require("../src/top");
read = function(file) { return JSON.parse(require("fs").readFileSync(file)); }

non_repeated = reasoner(read("test/c.json"),       [read("test/core.json"), read("test/sample_degree.json")]);
repeated     = reasoner(read("test/courses.json"), [read("test/core.json"), read("test/sample_degree.json")]);

function test(variable, value) {
  console.log(variable == value ? "Good" : "Bad");
}

test(non_repeated.hours, 29);
test(non_repeated.satisfied, false);
test(non_repeated.checked_subrequirements.length - non_repeated.checked_subrequirements.filter(sub => sub.satisfied).length, 7);

test(repeated.hours, 38);
test(repeated.satisfied, false);
test(repeated.checked_subrequirements.length - repeated.checked_subrequirements.filter(sub => sub.satisfied).length, 7);
