/* Course class
 * Attributes:
 * name: String, name of the course
 * min_hours: Minimum number of hours the course may provide
 * max_hours: Maximum number of hours the course may provide
 */

Base = require('./base.js');

class Course extends Base {
  constructor(course, min_grade, attempts) {
    super();
    this.name      = course.full_number;
    this.min_hours = course.minimum_credits;
    this.max_hours = course.maximum_credits;
    this.min_grade = min_grade;
    this.attempts  = attempts;
  }

  /* This is the satisfy method specified in the algorithm. Check whether or
   * not the student has taken the course, and whether or not the minimum grade
   * is satisfied.
   */
  vdash(student) {
    // A sort of ``empty'' requirement
    var replacement = new CheckedRequirement(this.name, this.min_hours, this.max_hours, 1, []);
    /* Find courses that matches this one, including min grade (simeq). Then,
     * sort so the highest grades come first. Take only as many as attempts are
     * required.
     */
    var student_courses = student.courses.filter(course => course.simeq(this))
      .sort((a, b) => b.grade.preceq(a.grade)).slice(0, (this.attempts || 1));
    if(student_courses.length) {
      // The hours should be the sum of the total
      replacement.hours = student_courses.map(course => course.hours).reduce((a, e) => a + e);
      // Take all courses that were use and pass them up
      replacement.subrequirements = student_courses;
      replacement.checked_subrequirements = student_courses;
    } else {
      // Nothing was satisfied, so pass a zero
      replacement.hours = 0;
    }
    replacement.kind = 'course';
    return replacement;
  }

}

module.exports = Course;
