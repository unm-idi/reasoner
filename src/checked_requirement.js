/* Attributes:
 * name: String, the name of the requirement
 * min_hours: Integer, the minimum number of hours to satisfy the requirement
 * max_hours: Integer, the maximum number of hours the requirment may provide
 * take: Integer, the minimum number of subrequirements to be satisfied
 * subrequirements: Array of subrequirements (requirements and courses)
 */

Base = require('./base.js');

class CheckedRequirement extends Base {
  constructor(name, min_hours, max_hours, take, subrequirements) {
    super();
    this.name                    = name;
    this.min_hours               = min_hours;
    this.max_hours               = max_hours;
    this.take                    = take;
    this.subrequirements         = subrequirements;
    this.checked_subrequirements = [];
  }

  /* The algorithm specifies a need to know whether or not a requirement is
   * ``basic''. This means that every subrequirement is a course.
  get basic() {
    return this.subs.every(sub => sub.type == 'Course');
  }
  */

  // How many subrequirements did the student satisfy?
  get taken() {
    return this.satisfied_subrequirements.length;
  }

  // How many credit hours is this subrequirement worth independently?
  // get abs() {
    // return this.subrequirements.map(sub => sub.abs).reduce((a, e) => a + e, 0); 
  // }

  // Is this requirement satisfied (represented as \vdash)?
  get satisfied() {
    return this.taken >= this.take && this.hours >= this.min_hours;
  }

  // A helper method to get all the satisfied subrequirements
  get satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => requirement.satisfied);
  }
  //
  // A helper method to get all the satisfied subrequirements
  get partially_satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => 
      !requirement.satisfied && (requirement.hours > 0 && requirement.taken > 0)
    );
  }
  
  // A helper method to get all the not satisfied subrequirements
  get not_satisfied_subrequirements() {
    return this.checked_subrequirements.filter(requirement => 
      !requirement.satisfied && !(requirement.hours > 0 && requirement.taken > 0)
    );
  }

  // The courses in this set, but not the other set (name ``requirement'')
  setminus(requirement) {
    return this.subrequirements.filter(course => requirement.subrequirements.indexOf(course) < 0);
  }

  // The courses in either this set, or the other set (named ``requirement'')
  cup(requirement) {
    return [...new Set([...this.subrequirements, ...requirement.subrequirements])];
  }

  // The courses in both this set, and the other set (named ``requirement'')
  cap(requirement) {
    return this.subrequirements.filter(course => requirement.subrequirements.indexOf(course) >= 0);
  }

  // Alias method for backwards compatibility
  get credit_hours_required() {
    return this.min_credit_hours;
  }

  // Alias method for backwards compatibility
  get hours_satisfied() {
    return this.hours;
  }

  // The merge (\merge) method. This is a binary operator.
  merge(requirement) {
    // Hours in this set or sum of course hours
    var hours = Math.min(Math.min(Base.abs(this.subrequirements), Base.abs(this.setminus(requirement))) +
        // Hours in the peer set or sum of course hours
        Math.min(requirement.hours, Base.abs(requirement.setminus(this))) +
        // Hours in courses that are in both
        Base.abs(this.cap(requirement)),
        // Sum of nominal hours of both requirements.
        this.max_hours + requirement.max_hours);
    // Subrequirements now has courses from both
    var subrequirements = this.cup(requirement);
    // Return the new requirement (probably going to keep merging)
    var replacement = new CheckedRequirement(this.name + ", " + requirement.name, hours, hours, 0, subrequirements);
    replacement.hours = hours;
    return replacement;
  }

  // Now that the unused courses have been filtered, reduce
  reason() {
    // Reason each one
    var replacement = this.subrequirements.map(sub => sub.reason())
      // Keep the satisfied ones
      .filter(sub => sub.satisfied)
      // Merge
      .reduce((acc, sub) => acc.merge(sub), new CheckedRequirement("", 0, 0, 0, []));
    return replacement;
  }

  toJSON() {
    var json_object = Base.json_from_keys(this);
    ['satisfied_subrequirements',
     'partially_satisfied_subrequirements',
     'not_satisfied_subrequirements',
     'taken',
     'satisfied',
     'credit_hours_required',
     'hours_satisfied'
    ].forEach(method => json_object[method] = this[method]);
    ['checked_subrequirements',
     'hours',
     'max_hours'
    ].forEach(key => delete(json_object[key]));
    json_object.type = String(this.kind || this.type).toLowerCase();
    return json_object;
  }

}

module.exports = CheckedRequirement;
