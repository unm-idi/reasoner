var Base               = require('./base.js');
var CheckedRequirement = require('./checked_requirement.js');
var Course             = require('./course.js');
var Grade              = require('./grade.js');
var Requirement        = require('./requirement.js');
var Rule               = require('./rule.js');
var Student            = require('./student.js');
var StudentCourse      = require('./student_course.js');
var Wildcard           = require('./wildcard.js');

function student_json(courses) {
  return new Student(courses);
}

function requirement_json(requirement_roots) {
  var requirements = requirement_roots.map(requirement_root =>
    new Requirement(
      "Requirement Root",
      requirement_root.total_credit_hours, 
      requirement_root.total_credit_hours, 
      requirement_root.min_grade || 'C',
      requirement_root.take || requirement_root.requirements.length,
      requirement_root.requirements
    )
  );
  var major = requirements.find(req => 
    req.min_hours == Math.max(...requirements.map(req => req.min_hours))
  );
  major.subrequirements = major.subrequirements.concat(requirements.filter(req => major != req));
  return major;
}

function reason(student, requirements) {
  return requirement_json(requirements).vdash(student_json(student));
}

// module.exports = { Base, CheckedRequirement, Course, Grade, Requirement, Student, StudentCourse, Wildcard, Rule, student_json, requirement_json, reason };
module.exports = reason;
