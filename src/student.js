/* Student class
 * Represents a student
 * Attributes:
 * name: String, name of the student (or alias)
 * courses: Array, courses that students have taken
 */

Base          = require("./base.js");
StudentCourse = require("./student_course.js");

class Student extends Base {
  constructor(courses) {
    super();
    this.courses = courses.map(course => new StudentCourse(course.name, course.hours, course.grade));
  }

  // Get the courses that are in common with a requirement
  cap(requirement) {
    return courses.filter(course => 
        requirement.subrequirements.map(c => course.simeq(c)) &&
        requirement.min_grade.preceq(course.grade)
    );
  }

  // Check if the student satisfied a requirement
  vdash(requirement) {
    var tree = requirement.vdash(this);
    return tree.satisfied;
  }
}

module.exports = Student;
