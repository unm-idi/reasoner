/* Requirement Class
 * This class is the requirements that come directly from the API.
 * Attributes:
 * name: String, the name of the requirement
 * min_hours: Integer, the minimum hours required to satisfy the requirement
 * max_hours: Integer, the maximum hours the requirement may provide
 * take: Integer, the number of subrequirements needed to satisfy this
 * subrequirements: Array of subrequirements (Requirements and Courses).
 */

Base               = require("./base.js");
Grade              = require("./grade.js");
CheckedRequirement = require("./checked_requirement.js");
Course             = require("./course.js");
Wildcard           = require("./wildcard.js");

class Requirement extends Base {
  constructor(name, min_hours, max_hours, min_grade, take, subrequirements) {
    super();
    // Take the arguments and use them as attributes
    this.name            = name;
    this.min_hours       = min_hours;
    this.max_hours       = max_hours;
    // Transform this into a Grade object
    this.min_grade       = new Grade(min_grade);
    this.take            = take;
    this.subrequirements = subrequirements;

    // Take the courses, and add the min_grade to each one
    this.courses = this.pre_courses.map(course => new Course(course.course, this.min_grade, course.attempts));

    this.wildcards = this.pre_wildcards.map(wildcard => 
      new Wildcard(
        wildcard.name,
        wildcard.minimum_credits,
        wildcard.maximum_credits,
        wildcard.min_grade,
        wildcard.rules
      )
    );

    // Change the subrequirements into Requirement objects
    this.subrequirements = this.subs.map(sub =>
      new Requirement(
        sub.name,
        sub.minimum_credits,
        sub.maximum_credits,
        sub.min_grade,
        (sub.take || 1),
        sub.subrequirements
      )
    );
  }

  /* The algorithm specifies a need to know whether or not a requirement is
   * ``basic''. This means that every subrequirement is a course.
   */
  get basic() {
    return this.subs.every(sub => sub.type.toLowerCase() == 'course');
  }

  // Only Course subrequirements, from the arguments
  get pre_courses() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'course');
  }

  // Only Requirement subrequirements
  get subs() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'requirement');
  }

  get pre_wildcards() {
    return this.subrequirements.filter(sub => sub.type.toLowerCase() == 'wildcard');
  }

  // We need all the subrequirements in a single array for reasoning
  get full_subrequirements() {
    return this.subrequirements.concat(this.wildcards).concat(this.courses);
  }

  /* This is the satisfy method specified in the algorithm. Return a
   * CheckedRequirement.
   */
  vdash(student) {
    // Run this method on every subrequirement
    var checked = this.full_subrequirements.map(sub => sub.vdash(student));
    // Filter out the unsatisfied ones and merge (don't actually filter for now)
    var merged = checked
      //.filter(sub => sub.satisfied)
      .reduce((acc, sub) => 
      acc.merge(sub), new CheckedRequirement("", 0, 0, this.take, [])
    );
    var replacement = new CheckedRequirement(
        this.name,
        this.min_hours,
        this.max_hours,
        this.take,
        merged.subrequirements
    );
    replacement.hours = Math.min(this.max_hours, merged.hours);
    replacement.checked_subrequirements = checked;
    return replacement;
  }

}

module.exports = Requirement;
