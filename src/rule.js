/* Rule class
 * These are the rules to be matched against for the wildcard
 * Attributes:
 * code_string: The name of the department(s) to be matched against
 * match_string: The string corresponding to the entire name(s) of the courses
 * minimum_match_number: The lowest course number to be matched against
 * maximum_match_number: The highest course number to be matched against
 * min_hours: The lowest acceptable hours value for a course
 * max_hours: The highest acceptable hours value for a course
 * exclude: Courses matching this rule should be excluded from the set
 */
Base = require("./base.js");

class Rule extends Base {
  constructor(
    code_string,
    match_string,
    minimum_match_number,
    maximum_match_number,
    min_hours,
    max_hours,
    exclude,
    min_grade
  ) {
    super();
    this.code_string          = code_string;
    this.match_string         = match_string;
    this.minimum_match_number = minimum_match_number;
    this.maximum_match_number = maximum_match_number;
    this.min_hours            = min_hours;
    this.max_hours            = max_hours;
    this.exclude              = exclude;
    this.min_grade            = min_grade;
  }

  // We are using "splats" here, convert them to regular expression syntax
  splat_to_regex(splat) {
    return splat.replace(/\*|\?/g, char => char == "*" ? ".*" : ".")
  }

  // Check if string matches a pattern
  match(string, splat) {
    return !string || !!(string.match(this.splat_to_regex(splat)));
  }

  number_between(course_number) {
    return (!this.minimum_match_number || this.minimum_match_number <= course_number) && 
           (!this.maximum_match_number || this.maximum_match_number >= course_number);
  }

  hours_between(course_hours) {
    return (!this.min_hours || this.min_hours <= course_hours) && 
           (!this.max_hours || this.max_hours >= course_hours);
  }

  // Check if a single course matches the rule
  simeq(course) {
    return [
      this.match(course.name.split(" ")[0], this.code_string),
      this.match(course.name, this.match_string),
      this.number_between(parseInt(course.name.split(" ").pop())),
      this.hours_between(course.hours),
      this.min_grade.preceq(course.grade)
    ].every(Boolean);
  }

  // Get all courses that match this wildcard
  matches(courses) {
    return courses.filter(course => this.simeq(course))
  }

}

module.exports = Rule;
