/* Grade class
 * Attributes:
 * letter: String, the letter grade
 * order: Integer, where this ranks in the partially ordered set
 */

Base  = require("./base.js");
Order = require("./grade_order.json");

class Grade extends Base {
  constructor(letter) {
    super();
    this.letter = letter;
    this.order  = Grade.grade_order(letter);
    this.cr     = letter == "CR"
  }

  // Compare this to see if requirement's grade precedes the student's grade
  preceq(grade) {
    return (this.order <= grade.order) && (this.cr || !grade.cr);
  }

  static grade_order(letter) {
    return Order[letter] || Order["C"];
  }
}

module.exports = Grade;
