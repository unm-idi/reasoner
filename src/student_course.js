/* StudentCourse Class
 * Attrubutes:
 * name: String, the name of the course
 * hours: The number of credit hours the student receives
 * grade: The grade the student received
 */

Base = require("./base.js");

class StudentCourse extends Base {
  constructor(name, hours, grade) {
    super();
    this.name      = name;
    this.hours     = hours;
    this.grade     = new Grade(grade);
  }

  // Check if this is the same as an API course
  simeq(course) {
    return this.name == course.name && course.min_grade.preceq(this.grade);
  }

  // Used as an alias for self, for polymorphism with merge method
  get subrequirements() {
    return [this];
  }

  // Used as an alias for true for the merge method
  get satisfied() {
    return true;
  }

  toJSON() {
    var json_object = Base.json_from_keys(this);
    json_object.grade = (this.grade && this.grade.letter) || 'Unknown';
    return json_object;
  }
}

module.exports = StudentCourse;
