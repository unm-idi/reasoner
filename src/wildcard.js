/* This contains Wildcards to match courses against. Each Wildcard is a set a of
 * rules, which contain attributes to match courses against.
 * Attributes:
 * name: String, the name of the Wildcard
 * min_hours: The minimum hours required to satisfy the Wildcard
 * max_hours: The maximum hours the Wildcard may provide to the Requirement
 * rules: An array of rules to be matches against
 */
Base               = require("./base.js");
Rule               = require("./rule.js");
CheckedRequirement = require("./checked_requirement.js");

class Wildcard extends Base {
  constructor(name, min_hours, max_hours, min_grade, rules) {
    super();
    this.name       = name;
    this.min_hours  = min_hours;
    this.max_hours  = max_hours;
    this.min_grade  = new Grade(min_grade);
    this.rules      = rules.map(rule =>
      new Rule(
        rule.code_string,
        rule.match_string,
        rule.minimum_match_number,
        rule.maximum_match_number,
        (rule.min_hours || 1),
        (rule.max_hours || this.max_hours),
        rule.exclude,
        this.min_grade
      )
    );
  }

  // Requirements to be included in the set
  get includes() {
    return this.rules.filter(rule => !rule.exclude);
  }

  // Requirements to be excluded from the set
  get excludes() {
    return this.rules.filter(rule => rule.exclude);
  }

  // Union of two course sets
  cup(set_one, set_two) {
    return [...new Set([...set_one, ...set_two])];
  }

  // Courses that match
  match(courses) {
    // Find all the courses to include
    var included_courses = this.includes.map(rule => rule.matches(courses)).reduce((a, e) => this.cup(a, e), []);
    // Find all the courses to exclude
    var excluded_courses = this.excludes.map(rule => rule.matches(courses)).reduce((a, e) => this.cup(a, e), []);
    // Return all the included courses without the excluded ones
    return included_courses.filter(course => excluded_courses.indexOf(course) < 0 && this.min_grade.preceq(course.grade));
  }

  vdash(student) {
    var replacement = new CheckedRequirement(this.name, this.min_hours, this.max_hours, 1, this.match(student.courses));
    replacement.hours = Base.abs(this.match(student.courses));
    replacement.checked_subrequirements = this.match(student.courses);
    replacement.kind = 'wildcard';
    return replacement;
  }

}

module.exports = Wildcard;
